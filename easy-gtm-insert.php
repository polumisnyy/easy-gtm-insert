<?php
/*
Plugin Name: Easy GTM insert 
Plugin URI: 
Description: Добавления GTM меток на сайте на WordPress
Author: Polumisnyy
Version: 1.0
*/

register_uninstall_hook( __FILE__, 'easy_gtm_insert_uninstall' );
function easy_gtm_insert_uninstall() {
	delete_option( 'eg_gtm_head' );
	delete_option( 'eg_gtm_body' );
}

class EasyGtm {

	public $fields;

	/*
	 * Class constructor
	 */
	public function __construct()
	{
		//array of fields
		$this->fields = array( 'eg_gtm_head' => 'GTM header code', 
			                   'eg_gtm_body' => 'GTM body code' );
		add_action( 'admin_init', array( $this , 'eg_register_fields' ) );
		add_action( 'wp_head', array( $this , 'eg_add_header_gtm' ) );
		add_action( 'wp_footer', array( $this , 'eg_add_body_gtm' ) );
	}

	
	/*
	 * Add new fields
	 */
	public function eg_register_fields()
	{
		$eg_fields = $this->fields;
		foreach ( $eg_fields as $name => $label ) {
			register_setting( 'general', $name );	
			add_settings_field(
				$name . '_id',
				'<label for="' . $name . '_id">' . __( $label , 'EasyGtm' ) . '</label>',
				array( $this, 'eg_fields_html' ),
				'general',
				'default', 
				array( 
					'id' => $name . '_id', 
					'option_name' => $name 
				)
			);
		}
	}
	

	/*
	 * HTML for gtm settings
	 */
	public function eg_fields_html( $val )
	{
		$id = $val['id'];
		$option_name = $val['option_name'];
		?>
			<textarea id="<? echo $id ?>" name="<? echo $option_name ?>"><? echo esc_attr( get_option( $option_name ) ); ?></textarea>	
		<?php
	}



	/*
	 * Add gtm code to header
	 */
	public function eg_add_header_gtm()
	{
		if( $gtm_header_code = get_option( 'eg_gtm_head' ) ) {
			echo  $gtm_header_code;
		}
	}


	/*
	 * Add gtm code to body
	 */
	public function eg_add_body_gtm()
	{
		if( $eg_gtm_body = get_option( 'eg_gtm_body' ) ) {
			echo  $eg_gtm_body;
		}
	}

}
$EasyGtm = new EasyGtm;